import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessageComponent } from './messages/message/message.component';
import { MessagesComponent } from './messages/messages.component';
import { MessageListComponent } from './messages/message-list/message-list.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactDetailComponent } from './contacts/contact-detail/contact-detail.component';
import { NewContactComponent } from './contacts/new-contact/new-contact.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { ContactSearchComponent } from './contacts/contact-search/contact-search.component';
import { EditContactComponent } from './contacts/edit-contact/edit-contact.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { AuthComponent } from './auth/auth.component';

const routes: Routes = [
  { path: '', component: WelcomeComponent, pathMatch: 'full'},
  { path: 'messages', component: MessagesComponent, children: [
    {path: 'inbox', component: MessageListComponent},
    {path: 'inbox/:messageid', component: MessageComponent},
  ]},
  {path: 'contacts', component: ContactsComponent, children: [
    {path: 'search', component: ContactSearchComponent},
    {path: 'new', component: NewContactComponent},
    {path: 'edit/:contactid', component: EditContactComponent},
    {path: ':contactid', component: ContactDetailComponent}
  ]},
  {path: 'preferences', component: PreferencesComponent},
  {path: 'login', component: AuthComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
