import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Contact } from '../contact.model';
import { ContactService } from '../contact.service';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.scss']
})
export class NewContactComponent implements OnInit {
  newContactForm: FormGroup;
  contacts: FirebaseListObservable<any[]>;

  constructor(private contactService: ContactService, af: AngularFire) { 
     this.contacts = af.database.list('/contacts');
  }

  ngOnInit() {
    this.newContactForm = new FormGroup({
      'fname': new FormControl(null, Validators.required),
      'lname': new FormControl(null, Validators.required),
      'address': new FormControl(null),
      'city': new FormControl(null),
      'state': new FormControl(null),
      'zip': new FormControl(null),
      'country': new FormControl(null),
      'telephone': new FormControl(null),
      'email': new FormControl(null),
      'company': new FormControl(null),
    })
  }
  onSubmit() {
    let newContact = new Contact(
      this.newContactForm.value.fname.trim(),
      this.newContactForm.value.lname.trim(),
      this.newContactForm.value.address,
      this.newContactForm.value.city,
      this.newContactForm.value.state,
      this.newContactForm.value.zip,
      this.newContactForm.value.country,
      this.newContactForm.value.telephone,
      this.newContactForm.value.email,
      this.newContactForm.value.company,
    );
    this.contacts.push(newContact);
  }
}
