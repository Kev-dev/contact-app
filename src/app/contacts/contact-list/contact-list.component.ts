import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContactService } from '../contact.service';
import { Subscription } from "rxjs/Subscription";
import { Contact } from '../contact.model';


@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})

export class ContactListComponent implements OnInit, OnDestroy {
  contactSubscription: Subscription;
  contacts: Contact[];

  constructor(private contactService: ContactService) {}

  ngOnInit() {
    this.contactSubscription = this.contactService.getContacts().subscribe(contacts => {
      this.contacts = contacts;
    });
  }

  ngOnDestroy() {
    this.contactSubscription.unsubscribe();
  }
}
