import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ContactService } from '../contact.service';
import { Contact } from '../contact.model';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss']
})
export class ContactDetailComponent implements OnInit, OnDestroy {
  contactDetails: Contact;
  paramSubscription;
  contactSubscription;
  contactId;
  
  CONTACT_API: string = 'https://contact-app-77443.firebaseio.com/contacts/';
  constructor(private route: ActivatedRoute, 
              private contactService: ContactService) { }

  ngOnInit() {
    this.paramSubscription = this.route.params.subscribe((params: Params) => {
      this.contactId = params['contactid'];
      this.contactSubscription = this.contactService.getContactDetails(this.contactId).subscribe(contactDetails => {
        this.contactDetails = contactDetails;
      });
    });
  }

  ngOnDestroy() {
    this.paramSubscription.unsubscribe();
    this.contactSubscription.unsubscribe();
  }
}
