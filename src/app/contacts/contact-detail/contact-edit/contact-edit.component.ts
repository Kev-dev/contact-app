import { Component, OnInit, Input } from '@angular/core';
import { ContactService } from '../../contact.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.scss']
})
export class ContactEditComponent implements OnInit {

  @Input()
  contact;
  @Input()
  contactKey;

  constructor(private contactService: ContactService, private router: Router) { }

  ngOnInit() {
  }

  removeContact(id) {
    if(!id) return;
    this.contactService.removeContact(id).then(() => {
      this.router.navigate(['/contacts/search']);
    });
  }

  editContact(contactKey) {
    if(!contactKey) return;
    this.router.navigate([`/contacts/edit/${contactKey}`]);
  }

  messageContact(contact) {
    console.log(`Sending message to ${contact.fname} ${contact.lname}`);
  }

}
