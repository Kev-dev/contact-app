export class Contact {
  public fname: string;
  public lname: string;
  public address: string;
  public city: string;
  public state: string;
  public zip: string;
  public country: string;
  public telephone: string;
  public email: string;
  public company: string;

  constructor(fname: string, lname: string, address: string, city: string, state: string, zip: string, country: string, telephone: string, email: string, company: string) {
    this.fname = fname;
    this.lname = lname;
    this.address = address;
    this.city = city;
    this.state = state;
    this.zip = zip;
    this.country = country;
    this.telephone = telephone;
    this.email = email;
    this.company = company;
  }
}