import { Injectable } from '@angular/core';
import { Contact } from './contact.model';
import { Subject } from 'rxjs/Rx';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Injectable()
export class ContactService {

  contactListChanged = new Subject();

  contacts: FirebaseListObservable<any[]>;

  constructor(private af: AngularFire) {
    this.contacts = af.database.list('/contacts');
  }


  getContacts() {
    return this.contacts.map(contact => contact);
  }

  getContactDetails(key: string) {
    return this.af.database.object(`https://contact-app-77443.firebaseio.com/contacts/${key}`);
  }

  addNewContact(contact: Contact) {
    this.contacts.push(contact);
  }

  removeContact(key: string) {
    if (!key) return;
    return this.contacts.remove(key);
  }

  editContact(key: string, contact: Contact) {
    this.contacts.update(key, contact);
  }

}