import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ContactService } from '../contact.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Contact } from '../contact.model';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.scss']
})
export class EditContactComponent implements OnInit {
  editContactForm: FormGroup;
  contactKey: string;
  paramSubscription: Subscription;
  contactSubscription: Subscription;
  constructor(private contactService: ContactService, private route: ActivatedRoute) { }
  ngOnInit() {
    this.editContactForm = new FormGroup({
      'fname': new FormControl(null, Validators.required),
      'lname': new FormControl(null, Validators.required),
      'address': new FormControl(null),
      'city': new FormControl(null),
      'state': new FormControl(null),
      'zip': new FormControl(null),
      'country': new FormControl(null),
      'telephone': new FormControl(null),
      'email': new FormControl(null),
      'company': new FormControl(null)
    })
    this.paramSubscription = this.route.params.subscribe((params: Params) => {
      this.contactKey = params['contactid'];
      this.contactSubscription = this.contactService.getContactDetails(this.contactKey)
        .subscribe(contact => {
          this.editContactForm.get('fname').setValue(contact.fname);
          this.editContactForm.get('lname').setValue(contact.lname);
          this.editContactForm.get('address').setValue(contact.address);
          this.editContactForm.get('city').setValue(contact.city);
          this.editContactForm.get('state').setValue(contact.state);
          this.editContactForm.get('zip').setValue(contact.zip);
          this.editContactForm.get('country').setValue(contact.country);
          this.editContactForm.get('telephone').setValue(contact.telephone);
          this.editContactForm.get('email').setValue(contact.email);
          this.editContactForm.get('company').setValue(contact.company);
        });
    })
  }

  onSubmit() {
    for (var key in this.editContactForm.value) {
      if (this.editContactForm.value.hasOwnProperty(key)) {
        if(!this.editContactForm.value[key]) {
          this.editContactForm.value[key] = null;
        }
      }
    }
    this.contactService.editContact(this.contactKey, this.editContactForm.value)
  }

  ngOnDestroy() {
    this.paramSubscription.unsubscribe();
    this.contactSubscription.unsubscribe();
  }

}
