import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable()
export class AuthService {

  token: string;
  email: string;
  constructor() {
  }

  login(email: string, password: string) {
    if (this.isAuthenticated()) {
      firebase.auth().signOut();
    }
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(response => {
        this.email = firebase.auth().currentUser.email;
        firebase.auth().currentUser.getToken().then(token => {
          this.token = token;
        });
      }).catch(err => {
        console.log(err);
      });
  }

  logout() {
    this.token = null;
    // this.email = null;
    firebase.auth().signOut();
  }

  isAuthenticated() {
    return this.token != null;
  }
}

