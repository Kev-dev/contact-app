import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit {
  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyA8bm1pj9WMrwmC9xLSmqY6RmbjprN-pi4",
      authDomain: "contact-app-77443.firebaseapp.com",
      databaseURL: "https://contact-app-77443.firebaseio.com"
    })
  }
}
