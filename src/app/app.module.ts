import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { MessagesComponent } from './messages/messages.component';
import { MessageCategoryComponent } from './messages/message-category/message-category.component';
import { MessageListComponent } from './messages/message-list/message-list.component';
import { MessageComponent } from './messages/message/message.component';
import { MessagesService } from 'app/messages/messages.service';
import { ContactsComponent } from './contacts/contacts.component';
import { ContactListComponent } from './contacts/contact-list/contact-list.component';
import { ContactDetailComponent } from './contacts/contact-detail/contact-detail.component';
import { ContactService } from './contacts/contact.service';
import { NewContactComponent } from './contacts/new-contact/new-contact.component';
import { AuthComponent } from './auth/auth.component';
import { ContactSearchComponent } from './contacts/contact-search/contact-search.component';
import { AngularFireModule } from 'angularfire2';
import { ContactEditComponent } from './contacts/contact-detail/contact-edit/contact-edit.component';
import { EditContactComponent } from './contacts/edit-contact/edit-contact.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { AuthService } from './auth/auth.service';

export const firebaseConfig = {
  apiKey: "AIzaSyA8bm1pj9WMrwmC9xLSmqY6RmbjprN-pi4",
  authDomain: "contact-app-77443.firebaseapp.com",
  databaseURL: "https://contact-app-77443.firebaseio.com",
  projectId: "contact-app-77443",
  messagingSenderId: "399511917060"
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    WelcomeComponent,
    MessagesComponent,
    MessageCategoryComponent,
    MessageListComponent,
    MessageComponent,
    ContactsComponent,
    ContactListComponent,
    ContactDetailComponent,
    NewContactComponent,
    AuthComponent,
    ContactSearchComponent,
    ContactEditComponent,
    EditContactComponent,
    PreferencesComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig)    
  ],
  providers: [MessagesService, ContactService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
