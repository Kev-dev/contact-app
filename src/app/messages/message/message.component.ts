import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MessagesService } from '../messages.service';
import { Message } from '../message.model';
import { Location } from '@angular/common'

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  message: Message;
  constructor(private route: ActivatedRoute, 
              private router: Router, 
              private messagesService: MessagesService,
              private location: Location) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      let messageId = params['messageid'];
      this.message = this.messagesService.getCurrentMessage(messageId)[0];
    })
  }

  onGoBack() {
    this.location.back();
  }

}
