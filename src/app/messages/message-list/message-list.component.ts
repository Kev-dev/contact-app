import { Component, OnInit } from '@angular/core';
import { Message } from '../message.model';
import { MessagesService } from '../messages.service';
import { Router } from '@angular/router';
import { element } from 'protractor';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss']
})
export class MessageListComponent implements OnInit {

  messages: Message[];
  constructor(private messageService: MessagesService, private router: Router) { }

  ngOnInit() {
    this.messages = this.messageService.getUserMessages();
  }

  onShowMessage(event, id) {
    if (event.target.tagName != 'TD') {
      this.messageService.removeMessages([0, 2]);
    }
    else {
      this.router.navigate([`/messages/inbox/${id}`])
    }
  }

  openEditMenu(element) {
    console.log(element);
    element.click();
  }
}
