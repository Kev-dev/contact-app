import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from "rxjs/Rx";
import 'rxjs/Rx';
import { Message } from './message.model';

@Injectable()
export class MessagesService {

  messages = [
    new Message('today@today.com', 'me@gmail.com', 'You received this message today!', Date.now().toString(), 1, false),
    new Message('unread@messaging.app', 'test@gmail.com', 'This is an unread message', '4/26/2017', 0, true),
    new Message('Zezima', 'me@gmail.com', 'From does not have to be an email', '4/26/2017', 2, false),
  ]

  getUserMessages() {
    return this.messages;
  }

  getCurrentMessage(id: number) {
    return this.messages.slice(id);
  }

  removeMessages(ids) {
    console.log('Commented out!');
    // this.messages = this.messages.filter(function (e) {
    //   return ids.indexOf(e.id) < 0;
    // });
  }
}