export class Message {
  public from: string;
  public recipient: string;
  public subject: string;
  public date: string;
  public id: number;
  public visited: boolean;
  constructor(from: string, recipient: string, subject: string, date: string, id: number, visited: boolean) {
    this.from = from;
    this.subject = subject;
    this.date = date;
    this.id = id;
    this.visited = visited
  }
}